# OpenHarmony for Waffle Nano

 <img src="docs/images/wafflenano1.png" height = "300" alt="Waffle Nano 1"/>
 <img src="docs/images/wafflenano2.png" height = "300" alt="Waffle Nano 2"/>

## 一、Waffle Nano 套件介绍

Waffle Nano 是一款基于 OpenHarmony 系统打造的创客套件。

- Type-C 接口实现云端编程、云端编译、在线烧录和云端调试。
- 主控为`Hi3861V100`：160MHz主频、SRAM 352KB、ROM 288KB、2M Flash。
- 板载 NFC 芯片及天线，可实现 OpenHarmony “碰一碰”功能。
- 板载九轴姿态传感，可实现姿态检测、计步等功能。
- 板载FPC底座可连接240*240分辨率TFT屏幕。

> 支持运行最新版本 OpenHarmony 和 Python 虚拟机
>
> 基于 `OpenHarmony-v1.1.1-lts` 定制的发行版固件还可支持运行精简的 `Python` 虚拟机，实现用 `Python` 开发 `OpenHarmony` 应用。

[产品介绍视频](https://www.bilibili.com/video/BV1cL411J7dE)

### 芯片参数
- Soc
    
    Hi3861V100

- CPU
    
    高性能 32bit 微处理器，最大工作频率 160MHz

- RAM
    
    内嵌 SRAM 352KB

- ROM
    
    ROM 288KB

- Falsh
    
    内嵌 2MB Flash

### 外设
- Wireless

    1x1 2.4GHz 频段（ch1～ch14）
    支持 IEEE802.11b/g/n 单天线所有的数据速率
    支持标准 20MHz 带宽和 5M/10M 窄带宽

- USB
    
    Type-C USB 供电、UART数据传输

- Display

    240*240 TFT LCD 屏幕

- UART 3个

- I2C 2个

- SPI 2个

- GPIO 15个

- SDIO 1个

- I2S 1个

- ADC输入 7路

- PWM 6路

## 二、使用说明

Waffle Nano 基于 OpenHarmony。可以使用 `黑胡桃动手实验室` 或者原生的 `DevEco Device Tool` 两种工具进行开发、编译、烧录、调测等。
当前 `DevEco Device Tool` 发布了 `Windows` 和 `Ubuntu` 两个版本，但是当前 `Windows` 版本（除 `Hi3861` 外）尚不支持在`Windows` 平台下编译，需要在 `Ubuntu` 平台下编译，所以推荐使用 `黑胡桃动手实验室` 进行云端编程、云端编译和在线烧写。

1.  开箱，安装外壳：[视频教程](https://www.bilibili.com/video/BV1j3411r7N8?from=search&seid=5645655502991799728)
2.  连接 Waffle Maker 云端编程平台开发
    - Python 开发：[视频教程](https://www.bilibili.com/video/BV1n64y1s7SW?from=search&seid=5645655502991799728)
    - C 开发：[视频教程](#)

## 三、源码目录简介

<table><thead align="left"><tr id="row198162047192810"><th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.1"><p id="p690319291299"><a name="p690319291299"></a><a name="p690319291299"></a>目录名</p>
</th>
<th class="cellrowborder" valign="top" width="50%" id="mcps1.2.3.1.2"><p id="p5903122962918"><a name="p5903122962918"></a><a name="p5903122962918"></a>描述</p>
</th>
</tr>
</thead>
<tbody><tr id="row1981674719280"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p69031429162912"><a name="p69031429162912"></a><a name="p69031429162912"></a>applications</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p090352912914"><a name="p090352912914"></a><a name="p090352912914"></a>Waffle Nano 应用程序样例</p>
</td>
</tr>
<tr id="row5816747132817"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p99031129112918"><a name="p99031129112918"></a><a name="p99031129112918"></a>base</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p790472962914"><a name="p790472962914"></a><a name="p790472962914"></a>基础软件服务子系统集&amp;硬件服务子系统集</p>
</td>
</tr>
<tr id="row1134218692910"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p4904112910295"><a name="p4904112910295"></a><a name="p4904112910295"></a>build</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p1090482942911"><a name="p1090482942911"></a><a name="p1090482942911"></a>组件化编译、构建和配置脚本</p>
</td>
</tr>
<tr id="row1841618902919"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p1390462902910"><a name="p1390462902910"></a><a name="p1390462902910"></a>domains</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p1390432914296"><a name="p1390432914296"></a><a name="p1390432914296"></a>增强软件服务子系统集</p>
</td>
</tr>
<tr id="row164164992915"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p79042298298"><a name="p79042298298"></a><a name="p79042298298"></a>foundation</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p18904132922915"><a name="p18904132922915"></a><a name="p18904132922915"></a>系统基础能力子系统集</p>
</td>
</tr>
<tr id="row1441610922915"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p490402916299"><a name="p490402916299"></a><a name="p490402916299"></a>kernel</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p1904112932912"><a name="p1904112932912"></a><a name="p1904112932912"></a>内核子系统</p>
</td>
</tr>
<tr id="row841718942913"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p12904929202919"><a name="p12904929202919"></a><a name="p12904929202919"></a>test</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p4904152912297"><a name="p4904152912297"></a><a name="p4904152912297"></a>测试子系统</p>
</td>
</tr>
<tr id="row24175915294"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p13904162992916"><a name="p13904162992916"></a><a name="p13904162992916"></a>third_party</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p6904829112917"><a name="p6904829112917"></a><a name="p6904829112917"></a>开源第三方组件</p>
</td>
</tr>
<tr id="row334210652914"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p1390442918299"><a name="p1390442918299"></a><a name="p1390442918299"></a>utils</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p690412296297"><a name="p690412296297"></a><a name="p690412296297"></a>常用的工具集</p>
</td>
</tr>
<tr id="row73421664298"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p7905172920292"><a name="p7905172920292"></a><a name="p7905172920292"></a>vendor</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p290510290293"><a name="p290510290293"></a><a name="p290510290293"></a>厂商提供的软件</p>
</td>
</tr>
<tr id="row734319617292"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p09056291290"><a name="p09056291290"></a><a name="p09056291290"></a>build.py</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p1790542912290"><a name="p1790542912290"></a><a name="p1790542912290"></a>编译脚本文件</p>
</td>
</tr>
<tr id="row734319617292"><td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.1 "><p id="p09056291290"><a name="p09056291290"></a><a name="p09056291290"></a>clean.sh</p>
</td>
<td class="cellrowborder" valign="top" width="50%" headers="mcps1.2.3.1.2 "><p id="p1790542912290"><a name="p1790542912290"></a><a name="p1790542912290"></a>清理脚本文件</p>
</td>
</tr>
</tbody>
</table>

## 四、开发板预览

![Waffle Nano V1 硬件介绍](docs/images/WaffleNanoV1_hardware.png)

## 五、关注我们

![BW Labs](https://gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document/raw/master/assets/bwqrcode.jpg)

## 六、参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 七、其他

- openharmony 兼容性认证通过

- [硬件原理图](https://gitee.com/blackwalnutlabs/openharmony-for-waffle-nano/blob/master/docs/WaffleNanoV1%E7%94%B5%E8%B7%AF%E5%8E%9F%E7%90%86%E5%9B%BE.pdf)

- [Python for OpenHarmony](https://gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document)

- [传感器驱动仓](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib)

- [技术支持](mailto:support@blackwalnut.tech)
