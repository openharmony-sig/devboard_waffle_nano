/* * Copyright (c) 2021 BlackWalnut Labs., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#define fp_barrierf fp_barrierf
// static inline float fp_barrierf(float x)
// {
// 	__asm__ __volatile__ ("" : "+w"(x));
// 	return x;
// }

// #define fp_barrier fp_barrier
// static inline double fp_barrier(double x)
// {
// 	__asm__ __volatile__ ("" : "+w"(x));
// 	return x;
// }

// #define fp_force_evalf fp_force_evalf
// static inline void fp_force_evalf(float x)
// {
// 	__asm__ __volatile__ ("" : "+w"(x));
// }

// #define fp_force_eval fp_force_eval
// static inline void fp_force_eval(double x)
// {
// 	__asm__ __volatile__ ("" : "+w"(x));
// }
